# *Mi Curriculum*

![images](images/gg7.png)

**Nom:** Musharaf Qaiser

**Mail:** y2396161n@iespoblenou.org


## Actualment

**Estudiant:** 2n SMIX



## Estudis

**PFI:** Insti de sant antoni


## Idiomes


Idioma | comprensió oral | comprensió escrita | expressió oral | expressió escrita 
:---:|:---:|:---:|:---:|:---:
Català |  bo |  bo |  bo |  bo
Castellà |  bo |  bo |  bo |  bo
Angles |molt bo | molt bo | molt bo | molt bo
Urdu | molt bo | molt bo | molt bo |molt bo


## Hobbies

 M'agrada jugar a videojocs i segueixo WWE.

## Capacitats transversals

- Treballador
- Responsable

## Futur i aspiracions

M'agradaria continuar els meus estudis:
- [x] Treure'm el 1r de SMIX.
- [x] Realitzar el cicle formatiu de grau superior de Informatica.
- [x] Treure'm el carnet de conduir
    